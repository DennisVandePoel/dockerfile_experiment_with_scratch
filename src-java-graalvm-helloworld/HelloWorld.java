public class HelloWorld {
    public static void main(String[] args) {
        String str = "Hello World! DVDP";
        System.out.println(str);
        System.out.println(reverseString(str));
        System.getenv().forEach((k, v) -> System.out.println(k + ": " + v));
    }

    public static String reverseString(String str) {
        if (str.isEmpty()) {
            return str;
        } else {
            return reverseString(str.substring(1)) + str.charAt(0);
        }        
    }
}