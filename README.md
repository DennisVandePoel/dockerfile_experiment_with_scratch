# Dockerfile_Experiment_With_Scratch

Playing around with static compilation and distroless OCI container images

Reasons for (not) going Distroless

- Distroless is primarily an optimization task: smaller images means less network io and storage
- Distroless is also a security improvement task: less executables usually means smaller attack surface

- Distroless requires the constant monitoring for CVE's or equivalent for all statically linked dependencies, and rebuilding the executables
