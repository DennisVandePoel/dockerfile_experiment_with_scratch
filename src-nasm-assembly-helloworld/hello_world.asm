; Simple hello world file
; Compile with NASM
; Date 20210126

global _start

section .text:

_start:
    mov eax, 0x04               ; 4 = sys_write
    mov ebx, 0x01               ; 1 = filedescriptor of the standard out stream
    mov ecx, message
    mov edx, message_length
    int 0x80                    ; invocation of the syscalls

    mov eax, 0x04               ; 4 = sys_write
    mov ebx, 0x02               ; 2 = filedescriptor of the standard error stream
    mov ecx, errmsg
    mov edx, errmsg_length
    int 0x80                    ; invocation of the syscalls

    mov eax, 0x1                ; 1 = sys_exit
    mov ebx, 0x0                ; Let's report 0 (success) as exit code
    int 0x80                    ; invocation of the syscalls

section .data:
    message: db "Hello World! DVDP", 0x0A          ; At the end append a ASCII LineFeed Character
    message_length equ $-message
    errmsg: db "Oh no! An Error occured!", 0x0A
    errmsg_length equ $-errmsg